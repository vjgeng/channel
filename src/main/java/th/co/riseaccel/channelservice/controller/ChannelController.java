package th.co.riseaccel.channelservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import th.co.riseaccel.channelservice.api.ChannelApi;
import th.co.riseaccel.channelservice.model.EventRequest;
import th.co.riseaccel.channelservice.model.EventResponse;
import th.co.riseaccel.channelservice.model.Welcome;
import th.co.riseaccel.channelservice.services.ChannelService;

//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
//@CrossOrigin
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RestController
public class ChannelController implements ChannelApi {

	private static final String welcomemsg = "Welcome Mr. %s!";

	@Autowired
	ChannelService channelService;

	@Override
	public Welcome welcomeUser(String name) {
		return channelService.welcomeUser(name);
	}

	@Override
	public EventResponse createEvent(String xApplicationName, EventRequest eventRequest) {
		return channelService.createEvent(xApplicationName, eventRequest);
	}

}
