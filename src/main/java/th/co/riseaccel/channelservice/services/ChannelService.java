package th.co.riseaccel.channelservice.services;

import th.co.riseaccel.channelservice.model.EventRequest;
import th.co.riseaccel.channelservice.model.EventResponse;
import th.co.riseaccel.channelservice.model.Welcome;

public interface ChannelService {

	public EventResponse createEvent(String xApplicationName, EventRequest eventRequest);

	public Welcome welcomeUser(String name);

}
