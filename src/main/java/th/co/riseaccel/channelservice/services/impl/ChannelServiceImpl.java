package th.co.riseaccel.channelservice.services.impl;

import org.apache.http.HttpHost;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import th.co.riseaccel.channelservice.model.EventRequest;
import th.co.riseaccel.channelservice.model.EventResponse;
import th.co.riseaccel.channelservice.model.Welcome;
import th.co.riseaccel.channelservice.services.ChannelService;

@Service("channelservice")
public class ChannelServiceImpl implements ChannelService {

	private static final String welcomemsg = "Welcome Mr. %s!";

	private static final Logger LOG = LoggerFactory.getLogger(ChannelServiceImpl.class);

	RestTemplate restTemplate = restTemplate();

	@Override
	public EventResponse createEvent(String xApplicationName, EventRequest eventRequest) {
		EventResponse eventResponse = new EventResponse();

		String url = "http://localhost:9104/v1/api/createRestuarant";
		String channel = "online";
		String oe = "TH";
		String form = "685";

		LOG.info("Status : " + eventRequest.getStatus());
		LOG.info("Event : " + eventRequest.getEvent());

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("channel", channel)
				.queryParam("oe", oe).queryParam("form", form);

		HttpHeaders headers = this.setupHeaders(xApplicationName);
		HttpEntity request = new HttpEntity(eventRequest, headers);

		ResponseEntity<EventResponse> out = restTemplate.exchange(url, HttpMethod.POST, request, EventResponse.class);

		LOG.info("HTTP BODY RESPONSE : " + out.getBody());
		LOG.info("HTTP STATUS RESPONSE : " + out.getStatusCode());

		LOG.info("End of Process");
		eventResponse = out.getBody();
		// eventResponse.setStatus("oke");
		// eventResponse.setEvent(eventRequest.getEvent());
		return eventResponse;
	}

	@Override
	public Welcome welcomeUser(String name) {
		return new Welcome(String.format(welcomemsg, name));
	}

	private HttpHeaders setupHeaders(String xApplicationName) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Application-Name", xApplicationName);
		return headers;
	}

	public RestTemplate restTemplate() {

		final String proxyUrl = "localhost";
		final int port = 9104;

		CredentialsProvider credsProvider = new BasicCredentialsProvider();

		HttpHost myProxy = new HttpHost(proxyUrl, port);
		HttpClientBuilder clientBuilder = HttpClientBuilder.create();

		clientBuilder.setProxy(myProxy).setDefaultCredentialsProvider(credsProvider).disableCookieManagement();

		HttpClient httpClient = clientBuilder.build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setHttpClient(httpClient);
		factory.setReadTimeout(3000000);
		factory.setConnectTimeout(3000000);
		factory.setConnectionRequestTimeout(3000000);

		return new RestTemplate(factory);
	}

}
