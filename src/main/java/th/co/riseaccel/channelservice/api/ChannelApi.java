package th.co.riseaccel.channelservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import th.co.riseaccel.channelservice.model.EventRequest;
import th.co.riseaccel.channelservice.model.EventResponse;
import th.co.riseaccel.channelservice.model.Welcome;

@RequestMapping(value = "/v1/api")
public interface ChannelApi {

	@GetMapping("/welcome/user")
	@ResponseBody
	public Welcome welcomeUser(@RequestParam(name = "name", required = false, defaultValue = "Java Fan") String name);

	@RequestMapping(value = "/createEvent", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	EventResponse createEvent(@RequestHeader("X-Application-Name") String xApplicationName,
			@RequestBody EventRequest eventRequest);

}
